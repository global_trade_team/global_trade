


function getCookie(name) {
    var matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
}

function addDays(date, days) {
    var result = new Date(date);
    result.setDate(result.getDate() + days);
    return result;
}

function CLEANCACHE() {
    var date_from_cache;

    chrome.storage.local.get(function(data) {
       for (var key in data) {
           if (key.includes('date'))
           {
               date_from_cache = data[key];
               var date = new Date(date_from_cache);
               if (addDays(date, 2) < new Date())
                   chrome.storage.local.remove([key, key.split("date")[0]], function () {
                       console.log(key.split("date")[0], "removed from cache");
                   });
           }
       }
    });
}
CLEANCACHE();